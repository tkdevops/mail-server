# mail-server

docker mail-server with Let's encrypt service
URL: https://github.com/docker-mailserver/docker-mailserver

- ตัวไฟล์ docker-compose.yml ไม่ต้อง donwload แต่ให้ใช้ตัวล่าสุด
- ทำงานคู่กับ nginx-reverseproxy-aceme แล้ว
- ตัว cname ที่ dns จะต้องตั้งค่าตรง  ห้ามผ่าน proxy ของ cloudflare มิเช่นนั้นจะ invalid

## Getting started

```
DMS_GITHUB_URL='https://raw.githubusercontent.com/docker-mailserver/docker-mailserver/master'
wget "${DMS_GITHUB_URL}/mailserver.env"
wget "${DMS_GITHUB_URL}/setup.sh"
chmod a+x ./setup.sh
```

## basic command
```
./setup.sh email add user1@docker.example.com
./setup.sh email del user1@docker.example.com
./setup.sh email update user1@docker.example.com
./setup.sh email list

# dkim config (after create first account)
./setup.sh config dkim
```

## setup DKIM
```
Configuration using a Web Interface
content of DKIM "docker-data/dms/config/opendkim/keys/example.com/mail.txt"
1 go to cloudflare and DNS setup
2 Paste "mail._domainkey" the Name txt field.
3 In the 'Content' or 'Target' or 'Value' field fill in " v=DKIM1; k=rsa; p=AZERTYUGHJKLMWX....... == "
4 Save and wait 5 to 1 hour
```
